/** @type {import('tailwindcss').Config} */
const colors = require("tailwindcss/colors.js");
module.exports = {  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
    fontFamily: {
      sans: ['Poppins', 'sans-serif'],
    },
    colors: {
      black: colors.black,
      gray: colors.gray,
      white: colors.white,
      teal: "#4FD1C5",
      green: {
        '300': '#68D391',
        '400': '#48BB78',
      },
      'dark-blue': '#313860',
      dark: '#151928',
    }
  },
  plugins: [],
}
